import { createStore } from "vuex";

export default createStore({
  state: {
    todos: [],
  },

  mutations: {
    ADD_TODO(state, todo) {
      if (state.todos[state.todos.length - 1]) {
        let id = parseInt(state.todos[state.todos.length - 1].id) + 1;
        todo.id = `${id}`;
      } else {
        todo.id = "1";
      }
      state.todos.push(todo);
    },
    SET_STATUS(state, info) {
      state.todos.forEach((e) => {
        if (e.id == info.id) {
          e.status = info.status;
        }
      });

      const myTODO = JSON.stringify(state.todos);
      localStorage.setItem("todos", myTODO);
    },
    SET_ALL_TODOS(state, info) {
      state.todos = info;
    },

    DELETE_TODO_BY_ID(state, id) {
      for (var i = 0; i < state.todos.length; i++) {
        if (state.todos[i].id == id) {
          state.todos.splice(i, 1);
        }
      }
      const myTODO = JSON.stringify(state.todos);
      localStorage.setItem("todos", myTODO);
    },
  },

  actions: {
    async addTodo({ commit }, todo) {
      if (todo) {
        commit("ADD_TODO", todo);
      }
    },
    async setStatus({ commit }, info) {
      if (info) {
        commit("SET_STATUS", info);
      }
    },
    async setAllTodos({ commit }, info) {
      if (info) {
        commit("SET_ALL_TODOS", info);
      }
    },
    async backToDoing({ commit }, id) {
      if (id) {
        let info = {
          status: "doing",
          id: id,
        };
        commit("SET_STATUS", info);
      }
    },
    async deleteTodo({ commit }, id) {
      if (id) {
        commit("DELETE_TODO_BY_ID", id);
      }
    },
  },
  getters: {
    getTodos: (state) => {
      return state.todos;
    },
  },
  modules: {},
});
