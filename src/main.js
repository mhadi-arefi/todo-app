import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueToast from "vue-toast-notification";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "vue-toast-notification/dist/theme-sugar.css";

createApp(App).use(store).use(router).use(VueToast).mount("#app");
